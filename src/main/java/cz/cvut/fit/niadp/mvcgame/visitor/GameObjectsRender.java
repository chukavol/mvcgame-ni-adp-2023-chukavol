package cz.cvut.fit.niadp.mvcgame.visitor;

import cz.cvut.fit.niadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsMissile;
import javafx.scene.paint.Color;

public class GameObjectsRender implements IGameObjectsVisitor {
    private IGameGraphics gameGraphics;

    public void setGraphicsContext(IGameGraphics gameGraphics) {
        this.gameGraphics = gameGraphics;
    }

    @Override
    public void visitCannon(AbsCannon cannon) {
        this.gameGraphics.drawImage(MvcGameConfig.CANNON_IMAGE_RESOURCE, cannon.getPosition());
    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.gameGraphics.drawImage((MvcGameConfig.MISSILE_IMAGE_RESOURCE), missile.getPosition());
    }

    @Override
    public void visitGameInfo(AbsGameInfo info) {
        this.gameGraphics.drawText(info.getGameInfo(), info.getPosition(), Color.GREY);
        this.gameGraphics.drawRectangle(
                new Position(info.getPosition().getX() - 10, info.getPosition().getY() - 30),
                new Position(MvcGameConfig.MAX_X - 10, info.getPosition().getY() + 190));

        if (info.allEnemiesDestroyed()) {
            this.gameGraphics.drawText("YOU WIN!", new Position(MvcGameConfig.MAX_X / 2 - 200, MvcGameConfig.MAX_Y / 2 - 100), Color.GREEN);
        } else if (info.allMissilesUsed()) {
            this.gameGraphics.drawText("YOU LOSE.", new Position(MvcGameConfig.MAX_X / 2 - 200, MvcGameConfig.MAX_Y / 2 - 100), Color.RED);
        }
    }

    @Override
    public void visitCollision(AbsCollision collision) {
        this.gameGraphics.drawImage(collision.getCollisionImage(), collision.getPosition());
    }

    public void visitEnemy(AbsEnemy enemy) {
        this.gameGraphics.drawImage(enemy.getEnemyImage(), enemy.getPosition());
    }
}
