package cz.cvut.fit.niadp.mvcgame.iterator;

import cz.cvut.fit.niadp.mvcgame.model.ThemeModel;

public interface ThemeIterator {

    ThemeModel getNext();

    ThemeModel getCurrent();

}
