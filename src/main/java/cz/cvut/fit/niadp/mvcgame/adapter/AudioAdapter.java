package cz.cvut.fit.niadp.mvcgame.adapter;

import cz.cvut.fit.niadp.mvcgame.iterator.ThemeIterator;
import cz.cvut.fit.niadp.mvcgame.model.enums.Audio;

public class AudioAdapter {

    private final AdvancedAudioPlayer audioPlayer;

    private boolean audioON;

    private ThemeIterator themeIterator;

    public AudioAdapter(ThemeIterator themeIterator) {
        this.audioPlayer = new AdvancedAudioPlayerImpl();
        this.themeIterator = themeIterator;
        this.audioON = true;
        playAudio(themeIterator.getCurrent().getAudio());
    }

    public void playAudio(Audio audio) {
        if(audioON) {
            switch (audio) {
                case AUDIO_A -> audioPlayer.playAudioA();
                case AUDIO_B -> audioPlayer.playAudioB();
            }
        }
    }

    public void toggleAudio() {
        audioON = !audioON;
        if (audioON) {
            playAudio(themeIterator.getCurrent().getAudio());
        } else {
            audioPlayer.stopPlaying();
        }
    }
}
