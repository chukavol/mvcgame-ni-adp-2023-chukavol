package cz.cvut.fit.niadp.mvcgame.model.enums;

public enum Theme {
    DARK,
    LIGHT
}
