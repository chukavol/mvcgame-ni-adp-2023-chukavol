package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.IGameModel;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.state.SingleShootingMode;
import cz.cvut.fit.niadp.mvcgame.strategy.SimpleMovingStrategy;
import cz.cvut.fit.niadp.mvcgame.visitor.IGameObjectsVisitor;

import java.util.Random;

public abstract class AbsGameInfo extends GameObject{

    private final IGameModel model;
    private final int allEnemies;
    private final int allMissiles;
    private int missilesUsed;

    public AbsGameInfo(IGameModel model) {
        this.model = model;
        this.missilesUsed = 0;
        this.allEnemies = model.getEnemies().size();
        this.allMissiles = allEnemies * new Random().nextInt(3,8) + new Random().nextInt(5,100);
        this.position = new Position(MvcGameConfig.GAME_INFO_POS_X, MvcGameConfig.GAME_INFO_POS_Y);
    }

    public String getGameInfo() {
        return "Strategy:  " + getStrategy() + "\n" +
                "Mode:  " + getMode() + "\n" +
                "Power:  " + model.getCannon().power + "\n" +
                "Missiles used:  " + missilesUsed + "/" + allMissiles + "\n" +
                "Enemies:  " + getEnemiesHit() + "/" + allEnemies + "\n" +
                "Score:  " + getScore();
    }

    public int getScore() {
        return Math.max(getEnemiesHit() * 50 - missilesUsed, 0);
    }

    private int getEnemiesHit() {
        return allEnemies - model.getEnemies().size();
    }

    public boolean allMissilesUsed() {
        return this.missilesUsed > this.allMissiles;
    }

    public boolean allEnemiesDestroyed() {
        return model.getEnemies().size() == 0;
    }
    public void incrementUsedMissiles() {
        this.missilesUsed++;
    }
    private String getMode() {
        return model.getShootingMode() instanceof SingleShootingMode ? "single" : "double";
    }

    private String getStrategy() {
        return model.getMovingStrategy() instanceof SimpleMovingStrategy ? "simple" : "realistic";
    }


    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitGameInfo(this);
    }
}
