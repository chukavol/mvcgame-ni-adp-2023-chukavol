package cz.cvut.fit.niadp.mvcgame.controller;

import cz.cvut.fit.niadp.mvcgame.command.AimCannonDownCommand;
import cz.cvut.fit.niadp.mvcgame.command.AimCannonUpCommand;
import cz.cvut.fit.niadp.mvcgame.command.MoveCannonDownCommand;
import cz.cvut.fit.niadp.mvcgame.command.MoveCannonUpCommand;
import cz.cvut.fit.niadp.mvcgame.command.CannonPowerDownCommand;
import cz.cvut.fit.niadp.mvcgame.command.CannonPowerUpCommand;
import cz.cvut.fit.niadp.mvcgame.command.PlayAudioCommand;
import cz.cvut.fit.niadp.mvcgame.command.RestartCommand;
import cz.cvut.fit.niadp.mvcgame.command.SwitchThemeCommand;
import cz.cvut.fit.niadp.mvcgame.command.ToggleMovingStrategyCommand;
import cz.cvut.fit.niadp.mvcgame.command.ToggleShootingModeCommand;
import cz.cvut.fit.niadp.mvcgame.command.UndoLastCommand;
import cz.cvut.fit.niadp.mvcgame.model.IGameModel;
import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;

import java.util.List;

public class GameController {

    private final IGameModel model;
    public GameController(IGameModel model) {
        this.model = model;
    }

    public void processPressedKeys(List<String> pressedKeysCodes) {
        for(String code : pressedKeysCodes) {
            switch(code) {
                case MvcGameConfig.UP_KEY:
                    this.model.registerCommand(new MoveCannonUpCommand(this.model));
                    break;
                case MvcGameConfig.DOWN_KEY:
                    this.model.registerCommand(new MoveCannonDownCommand(this.model));
                    break;
                case MvcGameConfig.SHOOT_KEY:
                    this.model.cannonShoot();
                    break;
                case MvcGameConfig.AIM_UP_KEY:
                    this.model.registerCommand(new AimCannonUpCommand(this.model));
                    break;
                case MvcGameConfig.AIM_DOWN_KEY:
                    this.model.registerCommand(new AimCannonDownCommand(this.model));
                    break;
                case MvcGameConfig.POWER_UP_KEY:
                    this.model.registerCommand(new CannonPowerUpCommand(this.model));
                    break;
                case MvcGameConfig.POWER_DOWN_KEY:
                    this.model.registerCommand(new CannonPowerDownCommand(this.model));
                    break;
                case MvcGameConfig.MOVING_STRATEGY_KEY:
                    this.model.registerCommand(new ToggleMovingStrategyCommand(this.model));
                    break;
                case MvcGameConfig.SHOOTING_MODE_KEY:
                    this.model.registerCommand(new ToggleShootingModeCommand(this.model));
                    break;
                case MvcGameConfig.UNDO_LAST_COMMAND_KEY:
                    this.model.registerCommand(new UndoLastCommand(this.model));
                    break;
                case MvcGameConfig.THEME_KEY:
                    this.model.registerCommand(new SwitchThemeCommand(this.model));
                    break;
                case MvcGameConfig.AUDIO_KEY:
                    this.model.registerCommand(new PlayAudioCommand(this.model));
                    break;
                case MvcGameConfig.RESTART_KEY:
                    this.model.registerCommand(new RestartCommand(this.model));
                    break;
                case MvcGameConfig.EXIT_KEY:
                    System.exit(0);
                    break;
                default:
                    //nothing
            }
        }
        this.model.update();
        pressedKeysCodes.clear();
    }
}
