package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.model.Position;


public class EnemyA extends AbsEnemy {

    public EnemyA(Position position) {
        super(position);
    }

    public EnemyA(EnemyA enemy) {
        super(enemy.position, enemy.enemyType, enemy.health);
    }

    @Override
    public AbsEnemy clone() {
        return new EnemyA(this);
    }
}
