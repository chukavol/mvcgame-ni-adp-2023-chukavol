package cz.cvut.fit.niadp.mvcgame.abstractfactory;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.IGameModel;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.CannonA;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.EnemyA;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.GameInfoA;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.MissileA;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameObjectsFactoryA implements IGameObjectsFactory {
    private IGameModel model;
    private static IGameObjectsFactory instance;

    public GameObjectsFactoryA(IGameModel model) {
        // TODO Singleton task
        this.model = model;
    }

    public GameObjectsFactoryA() {
        // TODO Singleton task
    }

    public static IGameObjectsFactory getInstance() {
        // TODO Singleton task
        return new GameObjectsFactoryA();
    }

    @Override
    public CannonA createCannon() {
        return new CannonA(new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), this);
    }

    @Override
    public MissileA createMissile(double initAngle, int initVelocity) {
        return new MissileA(
                new Position(this.model.getCannonPosition().getX(), this.model.getCannonPosition().getY()),
                initAngle,
                initVelocity,
                this.model.getMovingStrategy()
        );
    }

    @Override
    public AbsGameInfo createGameInfo(IGameModel model) {
        //TODO
        return new GameInfoA(model);
    }

    @Override
    public List<AbsEnemy> createEnemies() {
        int numberOfEnemies = new Random().nextInt(8, 16);
        List<AbsEnemy> list = new ArrayList<>();
        for (int i = 0; i < numberOfEnemies; i++) {
            int x = new Random().nextInt(400, MvcGameConfig.MAX_X - 400);
            int y = new Random().nextInt(300, MvcGameConfig.MAX_Y - 300);
            list.add(new EnemyA(new Position(x,y)));
        }
        return list;
    }
}
