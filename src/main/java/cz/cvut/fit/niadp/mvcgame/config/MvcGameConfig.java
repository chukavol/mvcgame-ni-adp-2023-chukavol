package cz.cvut.fit.niadp.mvcgame.config;

public class MvcGameConfig {

    public static final int MAX_X = 1920;
    public static final int MAX_Y = 1080;
    public static final int MOVE_STEP = 10;
    public static final int CANNON_POS_X = 200;
    public static final int CANNON_POS_Y = MAX_Y / 2;
    public static final int GAME_INFO_POS_X = 1600;
    public static final int GAME_INFO_POS_Y = 40;
    public static final double ANGLE_STEP = Math.PI / 18;
    public static final int POWER_STEP = 1;
    public static final int INIT_POWER = 10;
    public static final double INIT_ANGLE = 0;
    public static final double GRAVITY = 9.81;
    public static final int MAX_POWER = 50;
    public static final int MIN_POWER = 1;
    public static final long COLLISION_LIFETIME_MILLIS = 1000 * 3L;

    public static final String GAME_TITLE = "The NI-ADP MvcGame";

    public static final String UP_KEY = "UP";
    public static final String DOWN_KEY = "DOWN";
    public static final String EXIT_KEY = "ESCAPE";
    public static final String SHOOT_KEY = "SPACE";
    public static final String AIM_UP_KEY = "A";
    public static final String AIM_DOWN_KEY = "Y";
    public static final String POWER_UP_KEY = "F";
    public static final String POWER_DOWN_KEY = "D";
    public static final String MOVING_STRATEGY_KEY = "M";
    public static final String SHOOTING_MODE_KEY = "N";
    public static final String UNDO_LAST_COMMAND_KEY = "B";
    public static final String THEME_KEY = "T";
    public static final String AUDIO_KEY = "S";

    public static final String RESTART_KEY = "R";

    public static final String CANNON_IMAGE_RESOURCE = "images/cannon.png";

    public static final String MISSILE_IMAGE_RESOURCE = "images/missile.png";

    public static final String BACK_IMAGE_RESOURCE = "images/back.jpg";

    public static final String BACK_NIGHT_IMAGE_RESOURCE = "images/back_n.png";

    public static final String COLLISION_IMAGE_RESOURCE = "images/collision.png";

    public static final String ENEMY1_IMAGE_RESOURCE = "images/enemy1.png";

    public static final String ENEMY2_IMAGE_RESOURCE = "images/enemy2.png";

    public static final String ENEMY2_WITH_BLOOD_IMAGE_RESOURCE = "images/enemy2WithBlood.png";

    public static final String DARK_THEME_AUDIO = "build/resources/main/audio/audioB.mp3";

    public static final String LIGHT_THEME_AUDIO = "build/resources/main/audio/audioA.mp3";

}