package cz.cvut.fit.niadp.mvcgame.bridge;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.iterator.ThemeIterator;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class JavaFxGraphics implements IGameGraphicsImplementor {

    private final GraphicsContext gr;

    private final ThemeIterator iterator;


    public JavaFxGraphics(GraphicsContext gr, ThemeIterator iterator) {
        this.gr = gr;
        this.iterator = iterator;
    }

    @Override
    public void drawImage(String path, Position position) {
        this.gr.drawImage(new Image(path), position.getX(), position.getY());
    }

    @Override
    public void drawText(String text, Position position, Color color) {
        this.gr.setFont(Font.font("Chalkduster", FontWeight.EXTRA_BOLD, 25));
        this.gr.setFill(color);
        this.gr.fillText(text, position.getX(), position.getY());
    }

    @Override
    public void drawLine(Position beginPosition, Position endPosition) {
        this.gr.setStroke(Color.BLACK);
        this.gr.setLineWidth(0.4);
        this.gr.strokeLine(beginPosition.getX(), beginPosition.getY(), endPosition.getX(), endPosition.getY());
    }

    @Override
    public void clear() {
        this.gr.clearRect(0, 0, MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y);
        this.gr.drawImage(iterator.getCurrent().getBackImage(), 0, 0, MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y);
    }

    @Override
    public ThemeIterator getThemeIterator() {
        return iterator;
    }
}
