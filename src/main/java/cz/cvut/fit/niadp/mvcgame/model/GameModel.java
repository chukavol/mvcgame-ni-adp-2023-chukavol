package cz.cvut.fit.niadp.mvcgame.model;

import cz.cvut.fit.niadp.mvcgame.abstractfactory.GameObjectsFactoryA;
import cz.cvut.fit.niadp.mvcgame.abstractfactory.IGameObjectsFactory;
import cz.cvut.fit.niadp.mvcgame.adapter.AudioAdapter;
import cz.cvut.fit.niadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.niadp.mvcgame.iterator.ThemeIterator;
import cz.cvut.fit.niadp.mvcgame.model.enums.EnemyType;
import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.memento.CareTaker;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.CollisionA;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.niadp.mvcgame.observer.IObserver;
import cz.cvut.fit.niadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.niadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.niadp.mvcgame.strategy.RealisticMovingStrategy;
import cz.cvut.fit.niadp.mvcgame.strategy.SimpleMovingStrategy;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class GameModel implements IGameModel {

    private AbsCannon cannon;
    private final Set<IObserver> observers;
    private IGameObjectsFactory gameObjectsFactory;
    private final List<AbsMissile> missiles;
    private List<AbsEnemy> enemies;
    private List<AbsCollision> collisions;
    private AbsGameInfo gameInfo;
    private IMovingStrategy movingStrategy;
    private final Queue<AbstractGameCommand> unexecutedCommands;
    private final Stack<AbstractGameCommand> executedCommands;
    private ThemeIterator themeIterator;

    private AudioAdapter audioAdapter;

    public GameModel() {
        this.observers = new HashSet<>();
        this.gameObjectsFactory = new GameObjectsFactoryA(this);
        this.cannon = this.gameObjectsFactory.createCannon();
        this.missiles = new ArrayList<>();
        this.collisions = new ArrayList<>();
        this.enemies = this.gameObjectsFactory.createEnemies();
        this.movingStrategy = new SimpleMovingStrategy();
        this.unexecutedCommands = new LinkedBlockingQueue<>();
        this.executedCommands = new Stack<>();
        this.gameInfo = this.gameObjectsFactory.createGameInfo(this);
    }

    public void setThemeIterator(ThemeIterator iterator){
        this.themeIterator = iterator;
        this.audioAdapter = new AudioAdapter(iterator);
    }

    public void toggleAudio() {
        this.audioAdapter.toggleAudio();
    }

    @Override
    public void restart() {
        this.collisions.clear();
        this.missiles.clear();
        this.cannon = gameObjectsFactory.createCannon();
        this.executedCommands.clear();
        this.unexecutedCommands.clear();
        CareTaker.getInstance().clear();
        this.movingStrategy = new SimpleMovingStrategy();
        this.notifyObservers();
        this.enemies = gameObjectsFactory.createEnemies();
        this.gameInfo = gameObjectsFactory.createGameInfo(this);
    }

    public void switchTheme() {
        this.themeIterator.getNext();
        this.audioAdapter.playAudio(themeIterator.getCurrent().getAudio());
    }

    public void update() {
        this.executeCommands();
        this.moveMissiles();
    }

    private void executeCommands() {
        while (!this.unexecutedCommands.isEmpty()) {
            AbstractGameCommand command = this.unexecutedCommands.poll();
            command.doExecute();
            this.executedCommands.add(command);
        }
    }

    private void moveMissiles() {
        this.missiles.forEach(AbsMissile::move);
        this.destroyCollisions();
        this.destroyEnemies();
        this.destroyMissiles();
        this.notifyObservers();
    }

    private void destroyEnemies() {
        List<AbsEnemy> toRemove = new ArrayList<>();
        this.enemies.forEach(enemy -> {
                    if (enemy.isDead(this.missiles)) {
                        toRemove.add(enemy);
                        if (enemy.getEnemyType() == EnemyType.BASIC_PIG_1)
                            collisions.add(new CollisionA(enemy.getPosition()));
                    }
                }
        );
        this.enemies.removeAll(toRemove);
    }

    private void destroyCollisions() {
        this.collisions.removeIf(AbsCollision::readyToDie);
    }

    private void destroyMissiles() {
        this.missiles.removeAll(
                this.missiles.stream().filter(missile -> missile.getPosition().getX() > MvcGameConfig.MAX_X).toList()
        );
    }

    public Position getCannonPosition() {
        return this.cannon.getPosition();
    }

    public void moveCannonUp() {
        this.cannon.moveUp();
        this.notifyObservers();
    }

    public void moveCannonDown() {
        this.cannon.moveDown();
        this.notifyObservers();
    }

    public void cannonShoot() {
        if(!gameInfo.allMissilesUsed() && !gameInfo.allEnemiesDestroyed()) {
            this.missiles.addAll(this.cannon.shoot());
            this.gameInfo.incrementUsedMissiles();
            this.notifyObservers();
        }
    }

    public void aimCannonUp() {
        this.cannon.aimUp();
        this.notifyObservers();
    }

    public void aimCannonDown() {
        this.cannon.aimDown();
        this.notifyObservers();
    }

    public void cannonPowerUp() {
        this.cannon.powerUp();
        this.notifyObservers();
    }

    public void cannonPowerDown() {
        this.cannon.powerDown();
        this.notifyObservers();
    }

    @Override
    public void registerObserver(IObserver observer) {
        this.observers.add(observer);
    }

    @Override
    public void unregisterObserver(IObserver observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        this.observers.forEach(IObserver::update);
    }

    public List<AbsMissile> getMissiles() {
        return this.missiles;
    }

    @Override
    public List<AbsEnemy> getEnemies() {
        return this.enemies;
    }

    public AbsCannon getCannon() {
        return this.cannon;
    }

    public List<GameObject> getGameObjects() {
        List<GameObject> objects = new ArrayList<>(this.missiles);
        objects.add(this.cannon);
        objects.add(this.gameInfo);
        objects.addAll(this.enemies);
        objects.addAll(this.collisions);
        return objects;
    }

    public IMovingStrategy getMovingStrategy() {
        return this.movingStrategy;
    }

    public IShootingMode getShootingMode() {
        return this.cannon.getShootingMode();
    }

    public void toggleMovingStrategy() {
        if (this.movingStrategy instanceof SimpleMovingStrategy) {
            this.movingStrategy = new RealisticMovingStrategy();
        } else if (this.movingStrategy instanceof RealisticMovingStrategy) {
            this.movingStrategy = new SimpleMovingStrategy();
        } else {

        }
    }

    public void toggleShootingMode() {
        this.cannon.toggleShootingMode();
        this.notifyObservers();
    }

    static class Memento {
        private int cannonPositionX;
        private int cannonPositionY;
        private double cannonAngle;
        private int cannonPower;
        private IShootingMode shootingMode;
        private IMovingStrategy movingStrategy;
        private List<AbsEnemy> enemies;
        private AbsGameInfo gameInfo;
    }

    public Object createMemento() {
        Memento gameModelSnapshot = new Memento();
        gameModelSnapshot.cannonPositionX = this.cannon.getPosition().getX();
        gameModelSnapshot.cannonPositionY = this.cannon.getPosition().getY();
        gameModelSnapshot.cannonAngle = this.cannon.getAngle();
        gameModelSnapshot.cannonPower = this.cannon.getPower();
        gameModelSnapshot.shootingMode = this.cannon.getShootingMode();
        gameModelSnapshot.movingStrategy = this.movingStrategy;
        gameModelSnapshot.enemies = new ArrayList<>(this.enemies.stream().map(AbsEnemy::clone).toList());
        gameModelSnapshot.gameInfo = this.gameInfo;
        return gameModelSnapshot;
    }

    public void setMemento(Object memento) {
        Memento gameModelSnapshot = (Memento) memento;
        this.cannon.getPosition().setX(gameModelSnapshot.cannonPositionX);
        this.cannon.getPosition().setY(gameModelSnapshot.cannonPositionY);
        this.cannon.setAngle(gameModelSnapshot.cannonAngle);
        this.cannon.setPower(gameModelSnapshot.cannonPower);
        this.cannon.setShootingMode(gameModelSnapshot.shootingMode);
        this.enemies = gameModelSnapshot.enemies;
        this.movingStrategy = gameModelSnapshot.movingStrategy;
        this.gameInfo = gameModelSnapshot.gameInfo;
    }

    @Override
    public void registerCommand(AbstractGameCommand command) {
        this.unexecutedCommands.add(command);
    }

    @Override
    public void undoLastCommand() {
        if (!this.executedCommands.isEmpty()) {
            this.executedCommands.pop().unExecute();
            this.notifyObservers();
        }
    }
}
