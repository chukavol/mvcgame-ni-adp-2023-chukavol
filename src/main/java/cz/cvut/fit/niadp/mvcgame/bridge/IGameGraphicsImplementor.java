package cz.cvut.fit.niadp.mvcgame.bridge;

import cz.cvut.fit.niadp.mvcgame.iterator.ThemeIterator;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import javafx.scene.paint.Color;

public interface IGameGraphicsImplementor {
    void drawImage(String path, Position position);
    void drawText(String text, Position position, Color color);
    void drawLine(Position beginPosition, Position endPosition);
    void clear();
    ThemeIterator getThemeIterator();
}
