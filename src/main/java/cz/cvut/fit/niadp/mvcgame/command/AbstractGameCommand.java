package cz.cvut.fit.niadp.mvcgame.command;

import cz.cvut.fit.niadp.mvcgame.model.IGameModel;
import cz.cvut.fit.niadp.mvcgame.memento.CareTaker;

public abstract class AbstractGameCommand {

    protected IGameModel subject;

    protected abstract void execute();

    public void doExecute() {
        if(!(this instanceof UndoLastCommand)) {
            CareTaker.getInstance().createMemento();
        }
        this.execute();
    }

    public void unExecute() {
        CareTaker.getInstance().setMemento();
    }

}
