package cz.cvut.fit.niadp.mvcgame.iterator;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.ThemeModel;
import cz.cvut.fit.niadp.mvcgame.model.enums.Audio;
import cz.cvut.fit.niadp.mvcgame.model.enums.Theme;

public class ThemeIteratorImpl implements ThemeIterator {
    private Theme theme = Theme.LIGHT;

    private ThemeModel model = createModel(theme);

    @Override
    public ThemeModel getNext() {
        switch (theme) {
            case LIGHT -> {
                theme = Theme.DARK;
                model = createModel(Theme.DARK);
            }
            case DARK -> {
                theme = Theme.LIGHT;
                model = createModel(Theme.LIGHT);
            }
        }
        return model;
    }

    @Override
    public ThemeModel getCurrent() {
        return model;
    }

    private ThemeModel createModel(Theme theme) {
        if(theme == Theme.DARK) {
            return new ThemeModel(MvcGameConfig.BACK_NIGHT_IMAGE_RESOURCE, Audio.AUDIO_B);
        } else {
            return new ThemeModel(MvcGameConfig.BACK_IMAGE_RESOURCE, Audio.AUDIO_A);
        }
    }

}
