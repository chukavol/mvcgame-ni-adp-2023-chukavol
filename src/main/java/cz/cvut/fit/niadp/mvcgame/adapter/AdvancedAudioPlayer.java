package cz.cvut.fit.niadp.mvcgame.adapter;

public interface AdvancedAudioPlayer {

    void playAudioA();

    void playAudioB();

    void stopPlaying();

}
