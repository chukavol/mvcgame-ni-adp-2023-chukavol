package cz.cvut.fit.niadp.mvcgame.bridge;

import cz.cvut.fit.niadp.mvcgame.iterator.ThemeIterator;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import javafx.scene.paint.Color;

public class GameGraphics implements IGameGraphics {

    private final IGameGraphicsImplementor implementor;

    public GameGraphics(IGameGraphicsImplementor implementor) {
        this.implementor = implementor;
    }

    @Override
    public void drawImage(String path, Position position) {
        this.implementor.drawImage(path, position);
    }

    @Override
    public void drawText(String text, Position position, Color color) {
        this.implementor.drawText(text, position, color);
    }

    @Override
    public void drawRectangle(Position leftTop, Position rightBottom) {
        this.implementor.drawLine(leftTop, new Position(rightBottom.getX(), leftTop.getY()));
        this.implementor.drawLine(new Position(rightBottom.getX(), leftTop.getY()), rightBottom);
        this.implementor.drawLine(rightBottom, new Position(leftTop.getX(), rightBottom.getY()));
        this.implementor.drawLine(new Position(leftTop.getX(), rightBottom.getY()), leftTop);
    }

    @Override
    public void clear() {
        this.implementor.clear();
    }

    @Override
    public ThemeIterator getThemeIterator() {
        return this.implementor.getThemeIterator();
    }
}
