package cz.cvut.fit.niadp.mvcgame.model;

import cz.cvut.fit.niadp.mvcgame.model.enums.Audio;
import javafx.scene.image.Image;


public class ThemeModel {
    private final Image backImage;
    private final Audio audio;

    public ThemeModel(String backImagePath, Audio audio) {
        this.backImage = new Image(backImagePath);
        this.audio = audio;
    }

    public Image getBackImage() {
        return backImage;
    }

    public Audio getAudio() {
        return audio;
    }
}
