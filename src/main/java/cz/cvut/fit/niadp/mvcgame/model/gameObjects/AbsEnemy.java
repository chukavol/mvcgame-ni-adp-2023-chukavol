package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.model.enums.EnemyType;
import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.prototype.Prototype;
import cz.cvut.fit.niadp.mvcgame.visitor.IGameObjectsVisitor;

import java.util.List;
import java.util.Random;


public abstract class AbsEnemy extends GameObject implements Prototype {

    protected final EnemyType enemyType;

    protected int health;


    public AbsEnemy(Position position) {
        this.position = position;
        this.enemyType = EnemyType.getRandomType();
        this.health = (enemyType == EnemyType.BASIC_PIG_1) ? 1 : 2;
    }

    public AbsEnemy(Position position, EnemyType enemyType, int health) {
        this.position = position;
        this.enemyType = enemyType;
        this.health = health;
    }

    public String getEnemyImage() {
        return switch (enemyType) {
            case BASIC_PIG_1 -> MvcGameConfig.ENEMY1_IMAGE_RESOURCE;
            case MAGIC_PIG_2 -> health == 1 ? MvcGameConfig.ENEMY2_WITH_BLOOD_IMAGE_RESOURCE : MvcGameConfig.ENEMY2_IMAGE_RESOURCE;
        };
    }

    public EnemyType getEnemyType() {
        return enemyType;
    }

    public boolean isDead(List<AbsMissile> missiles) {
       return missiles.stream().anyMatch(missile -> {
           if (hitPosition(missile.position) && !missile.isDead()) {
               missile.markAsDead();
               this.health--;
               magicChangePosition();
           }
           return this.health == 0;
       });
    }

    public void recoverHealth() {
        health++;
    }

    private boolean hitPosition(Position position) {
        return Math.sqrt(Math.pow(this.position.getX() - position.getX(), 2) + Math.pow(this.position.getY() - position.getY(), 2)) <= 30;
    }

    private void magicChangePosition() {
        if(enemyType == EnemyType.MAGIC_PIG_2 && health == 1) {
            int x = new Random().nextInt(400, MvcGameConfig.MAX_X - 400);
            int y = new Random().nextInt(300, MvcGameConfig.MAX_Y - 300);
            this.position = new Position(x, y);
        }
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitEnemy(this);
    }

    public abstract AbsEnemy clone();
}
