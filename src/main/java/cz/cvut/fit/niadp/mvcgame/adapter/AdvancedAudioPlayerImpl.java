package cz.cvut.fit.niadp.mvcgame.adapter;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class AdvancedAudioPlayerImpl implements AdvancedAudioPlayer {

    private  MediaPlayer mediaPlayer;
    private final Media mediaA = new Media(new File(MvcGameConfig.LIGHT_THEME_AUDIO).toURI().toString());
    private final Media mediaB = new Media(new File(MvcGameConfig.DARK_THEME_AUDIO).toURI().toString());


    @Override
    public void playAudioA() {
        stopPlaying();
        this.mediaPlayer = new MediaPlayer(mediaA);
        this.mediaPlayer.setOnEndOfMedia(this::stopPlaying);
        this.mediaPlayer.play();
    }

    @Override
    public void playAudioB() {
        stopPlaying();
        this.mediaPlayer = new MediaPlayer(mediaB);
        this.mediaPlayer.setOnEndOfMedia(this::stopPlaying);
        this.mediaPlayer.play();
    }


    @Override
    public void stopPlaying() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.stop();
            this.mediaPlayer.dispose();
            this.mediaPlayer = null;
        }
    }
}
