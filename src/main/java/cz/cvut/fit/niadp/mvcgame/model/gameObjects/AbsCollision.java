package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.visitor.IGameObjectsVisitor;

import static cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig.COLLISION_LIFETIME_MILLIS;

public abstract class AbsCollision extends LifetimeLimitedGameObject {

    public AbsCollision(Position position) {
        super(position);
    }

    public String getCollisionImage() {
        return MvcGameConfig.COLLISION_IMAGE_RESOURCE;
    }


    public boolean readyToDie() {
        return COLLISION_LIFETIME_MILLIS <= getAge();
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitCollision(this);
    }
}
