package cz.cvut.fit.niadp.mvcgame.prototype;


public interface Prototype {
    Object clone();
}
