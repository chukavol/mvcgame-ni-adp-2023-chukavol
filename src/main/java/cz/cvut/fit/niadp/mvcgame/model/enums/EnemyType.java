package cz.cvut.fit.niadp.mvcgame.model.enums;

import java.util.Random;

public enum EnemyType {
    BASIC_PIG_1,
    MAGIC_PIG_2;

    public static EnemyType getRandomType() {
        return switch (new Random().nextInt(1, 3)) {
            case 1 -> BASIC_PIG_1;
            case 2 -> MAGIC_PIG_2;
            default -> throw new IllegalStateException("Unexpected value");
        };
    }
}
