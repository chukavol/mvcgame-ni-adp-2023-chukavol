package cz.cvut.fit.niadp.mvcgame.model;

import cz.cvut.fit.niadp.mvcgame.abstractfactory.GameObjectsFactoryA;
import cz.cvut.fit.niadp.mvcgame.abstractfactory.IGameObjectsFactory;
import cz.cvut.fit.niadp.mvcgame.model.GameModel;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.MissileA;
import cz.cvut.fit.niadp.mvcgame.strategy.SimpleMovingStrategy;
import mockit.MockUp;
import mockit.Mocked;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GameProrotypeTest {


    @Test
    public void testPrototype() {
        IGameObjectsFactory factory = new GameObjectsFactoryA();
        List<AbsEnemy> enemies = factory.createEnemies();

        List<AbsEnemy> shallowCopy = enemies;
        List<AbsEnemy> deepCopy = enemies.stream().map(it -> it.clone()).toList();

        enemies.remove(0);

        assertNotEquals(deepCopy, enemies);
        assertEquals(shallowCopy, enemies);
    }

}
