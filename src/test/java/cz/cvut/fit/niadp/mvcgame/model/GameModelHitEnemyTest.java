package cz.cvut.fit.niadp.mvcgame.model;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.MissileA;
import cz.cvut.fit.niadp.mvcgame.strategy.SimpleMovingStrategy;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GameModelHitEnemyTest {
    private static final int TEST_POSITION_X = 100;
    private static final int TEST_POSITION_Y = 200;

    @Mocked
    private AbsEnemy enemy;

    @Test
    public void testCollision() {
        this.generalMockSetup();
        Assert.assertEquals(enemy.getPosition().getX(), TEST_POSITION_X);

        List<AbsMissile> missiles = new ArrayList<>();
        missiles.add(new MissileA(new Position(TEST_POSITION_X, TEST_POSITION_Y), 0, 0, new SimpleMovingStrategy()));
        missiles.add(new MissileA(new Position(TEST_POSITION_X + 20, TEST_POSITION_Y), 0, 0, new SimpleMovingStrategy()));

        Assert.assertTrue(enemy.isDead(missiles));
        missiles.remove(0);
        Assert.assertFalse(enemy.isDead(missiles));
    }

    public void generalMockSetup() {
        new MockUp<AbsEnemy>() {
            @Mock
            public boolean isDead(List<AbsMissile> missiles) {
                return missiles.get(0).getPosition().getX() == TEST_POSITION_X;
            }

            @Mock
            public Position getPosition() {
                return new Position(TEST_POSITION_X, TEST_POSITION_Y);
            }
        };
    }
}
