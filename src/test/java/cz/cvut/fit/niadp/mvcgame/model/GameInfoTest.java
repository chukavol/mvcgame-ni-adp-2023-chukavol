package cz.cvut.fit.niadp.mvcgame.model;

import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.GameInfoA;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GameInfoTest {

    @Test
    public void gameInfoTest() {
        GameModel model = new GameModel();
        AbsGameInfo gameInfo = new GameInfoA(model);

        model.cannonShoot();
        model.cannonShoot();
        assertFalse(gameInfo.allMissilesUsed());

        for (int i = 0; i < 200; i++) {
            gameInfo.incrementUsedMissiles();
        }
        assertTrue(gameInfo.allMissilesUsed());
    }

}
