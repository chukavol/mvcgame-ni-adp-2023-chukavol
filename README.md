## mvcgame-ni-adp-2023 

### Run App

_./gradlew run_


___

### Features:

* Magic Pig - enemy number 2 teleports after first hit and changes the look.

* Themes - Dark/Light "T"

* Sound on/off - different based on the theme : "S"

* Restart option : "R"

* Limited umber of missiles. detection of WIN/LOSE after all enemies destroyed/ all missiles used.


### Design pattern:

* Adapter - audio player
* Iterator - theme switcher
* Prototype - enemy clone